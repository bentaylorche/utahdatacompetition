% This is an example for exponentialy weighted least squares regression. This is a Matlab/Octave script. For those without octave you can:
%     sudo apt-get install octave
%     octave WLSR_example.m
% Tweet me @bentaylordata with hashtag #utahdatacompetition with any questions

context_map=csvread('train_X_final.csv');             % Load train input (context matrix)
sim_yield_die_loss=csvread('train_Y_final.csv');      % Load train output
wafer_count=size(context_map,1);                % Get number of wafers

%Build regular WLSR model +++++++++++++++++++++++++++++++++++++++++++++++++
lambda=0.3;                                 %Decay rate on learning
w_diag=fliplr(lambda.^(1:wafer_count));
W=zeros(wafer_count);
for g=1:wafer_count
    W(g,g)=w_diag(g);
end
beta_WLSR=pinv(context_map'*W*context_map)*context_map'*W*sim_yield_die_loss';

context_map_val=csvread('val_X_final.csv');         %Load train input (context matrix)

%Evaluate models
yp_WLSR=context_map_val*beta_WLSR;                 

%Make sure to cap crazy results outside possible die >600 <0
yp_WLSR(yp_WLSR<0)=0;
yp_WLSR(yp_WLSR>600)=600;

%Now write out prediction
csvwrite('WLSR_prediction.csv',yp_WLSR)
