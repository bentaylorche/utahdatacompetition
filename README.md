# Data Competition Instructions

Contact: Ben Taylor, @bentaylordata, www.linkedin.com/in/bentaylordata/

**Host Leaderboard:**

| Author  | Score |
| ------------- | ------------- |
| [HOST-WLSR.m](https://bitbucket.org/bentaylorche/utahdatacompetition/src/924750f096b25baccbd7676783462fe4efffb029/WLSR_example.m?at=master)  | 322.466  |

**Quick start:**

First clone/download all of the content

```bash
git clone https://bentaylorche@bitbucket.org/bentaylorche/utahdatacompetition.git
cd utahdatacompetition
octave WLSR_example.m
```
This will produce a file called: [WLSR_prediction.csv](https://bitbucket.org/bentaylorche/utahdatacompetition/src/0d66037bf0748a690be93c7174fdb618aab02a83/WLSR_prediction.csv?at=master) that you can upload to www.gridisc.com to get a score ~322.

**Problem background:**
Semiconductor yield modeling can be very challenging because of all of the mixing that takes place inside a fab. Where is the problem coming from? This problem is also very valuable because excursions can quickly cost 10s of millions of dollars in lost revenue if not detected quickly. Can you design a model that can predict the next yield point based on the past days worth of fab production? The time effects are critical to understanding the system and predicting the next point. Ideally everyone would be scored on the next unseen point, but to make the rankings statistically significant we need to provide thousands of next points to compare. The great thing with a synthetic fab simulation is we can do this. So val_X.csv dataset includes 10,000 possible next points with their corresponding yield. For the final competition a new train_X.csv, train_Y.csv, val_X.csv datasets will be provided. For the final score val_Y.csv will not be given, but contestants can submit their prediction data sets for final scoring.


Files:

  * train_X_final.csv
  * train_Y_final.csv
  * val_X_final.csv

**train_X_final.csv:**
This file contains a binary matrix that is 9,999 rows by 2,000 columns. This matrix represents where each wafer ran moving through the fab during the past week. The 2,000 columns represent 2,000 unique contexts where the wafer could have run. These contests can represent tools, chambers, reticles, or any categorical input that could vary between wafers.

**train_Y_final.csv:**
This file contains the synthetic yield output generated by the wafers with 9,999. The maximum possible die for theses wafers is 600. So a value of 600 means all 600 die on the wafer failed, this is called a blackmap. A 0 would mean a perfect wafer, where all wafers passed, which is highly unlikely because of edge wafer effects.

##Predict the next point:
Because there is such a strong time factor the best metric that could be used to judge whether a model is working would be predicting the next wafer to yield. This would not be fair in a competition setting because a single observation prediction could be driven by luck. So to be fair, since this is a synthetic dataset, we can generate 10,000 potential inputs and yields that could have occurred for the next wafer.

**val_X_final.csv**
This file represents 10,000 different potential inputs for the very next wafer in the series. Run them through your model that has been trained using train_X.csv and train_Y.csv and compare them to val_Y.csv to score. For the final scoring a new dataset, which will be similar, will be given, where val_Y.csv is not provided but is used to score the entries.

**val_Y_final.csv**
This file represents the yield on the 10,000 potential wafers that could have run. This is the file you will use to compare your prediction against to score the value of your model. This file sits on the server at www.gridisc.com, you do not have direct access to this file, but you can use it for scoring by uploading your 10,000 prediction points. 

#**!!! IMPORTANT CUSTOM ERROR FUNCTION, THIS IS WHAT YOU ARE BEING SCORED ON**
This problem has a custom error function. This forces you to look at type-I and type-II errors in your modeling. So to do this any under prediction residuals get a 10x multiplier penalty added to them. Here is a Matlab example:

```
%Special cost function, under prediction is 10x penalty
err=Y_pred-Y_val;              %Calculate raw errors

%Custom residual sum
err_under=sum(abs(err(err<0)))*10;        %10x penalty
err_over=sum(abs(err(err>=0)));

overall_score_LSR=(err_under+err_over)/wafer_count
```

Here is a python example

```
# Calculate score
err_Y=Y_pred-Y_val
score = (sum(abs(err_Y[err_Y<0]))*10+sum(abs(err_Y[err_Y>=0])))/len(err_Y)
```




