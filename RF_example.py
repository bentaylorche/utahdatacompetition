import numpy
from numpy import genfromtxt
from sklearn.ensemble import RandomForestRegressor

# Load training data
X = genfromtxt('train_X_final.csv', delimiter=',')
Y = genfromtxt('train_Y_final.csv', delimiter=',')

# Create support vector regression object
regr = RandomForestRegressor(n_estimators=10)

# Train the model using the training sets
regr.fit(X, Y)

# Load validation data
X_val = genfromtxt('val_X_final.csv', delimiter=',')

# Now predict validation output
Y_pred = regr.predict(X_val)

# Crop impossible values
Y_pred[Y_pred < 0] = 0
Y_pred[Y_pred > 600] = 600

# save prediction for upload
numpy.savetxt("RF_prediction.csv", Y_pred, delimiter=",")
