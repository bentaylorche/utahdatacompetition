import numpy
from numpy import genfromtxt
from sklearn.svm import SVR

# Load training data
X = genfromtxt('train_X_final.csv', delimiter=',')
Y = genfromtxt('train_Y_final.csv', delimiter=',')

# Create support vector regression object
regr = SVR(C=1.0, epsilon=0.2)

# Train the model using the training sets
regr.fit(X, Y)

# Load validation data
X_val = genfromtxt('val_X_final.csv', delimiter=',')

# Now predict validation output
Y_pred = regr.predict(X_val)

# Crop impossible values
Y_pred[Y_pred < 0] = 0
Y_pred[Y_pred > 600] = 600

# save prediction for upload
numpy.savetxt("SVR_prediction.csv", Y_pred, delimiter=",")
